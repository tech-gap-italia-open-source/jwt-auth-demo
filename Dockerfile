FROM openjdk:8-jdk-alpine

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY target/*.jar app.jar

ENV JAVA_OPTS=""
ENV APP_ARGUMENTS=""
ENTRYPOINT ["sh","-c","java -Djava.security.egd=file:/dev/./urandom ${JAVA_OPTS} -jar app.jar ${APP_ARGUMENTS}"]
EXPOSE 8080
