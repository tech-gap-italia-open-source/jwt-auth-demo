package com.example;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

@RestController
public class UserController {

    public static final String ADMIN = "admin";
    private Map<String, UserDto> users = Collections.singletonMap(ADMIN, new UserDto(ADMIN, "Luca", "Rossi"));

    /* Maps to all HTTP actions by default (GET,POST,..)*/
    @RequestMapping("/profile")
    public @ResponseBody
    ResponseEntity<UserDto> getProfile(@AuthenticationPrincipal String activeUser) {
        return Optional.ofNullable(users.get(activeUser)).map(ResponseEntity::ok).orElse(ResponseEntity.notFound().build());
    }
}
